// Copyright Epic Games, Inc. All Rights Reserved.


#include "ThreadExamples2_hwGameModeBase.h"
#include <random>


void AThreadExamples2_hwGameModeBase::BeginPlay()
{
	Super::BeginPlay();

}

void AThreadExamples2_hwGameModeBase::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);
}

void AThreadExamples2_hwGameModeBase::CreateNames()
{
	InitListNames();
	int32 curSeletedNumber = 0;

	const auto FunctionGenerateName = [&](int32 index)
	{
		FPlatformProcess::Sleep(0.1f);

		// for (int i = 0; i < 50; ++i)
		// {			
			curSeletedNumber = 0;
			curSeletedNumber = GetRandomInt(0, 9);
		
			if (!ListNames[curSeletedNumber].IsEmpty())
			{
				All_Name.Enqueue(ListNames[curSeletedNumber]);
			}
		//}
	};

	ParallelForTemplate(NumberOfAttempts,FunctionGenerateName,EParallelForFlags::BackgroundPriority);
}

void AThreadExamples2_hwGameModeBase::CreateAges()
{
	int32 curSeletedNumber = 0;
	const auto FunctionGenerateName = [&](int32 index)
	{
		FPlatformProcess::Sleep(0.1f);

		// for (int i = 0; i < 50; ++i)
		// {			
			curSeletedNumber = 0;
			curSeletedNumber = GetRandomInt(20, 50);
		
			All_Age.Enqueue(curSeletedNumber);
		//}
	};

	ParallelForTemplate(NumberOfAttempts,FunctionGenerateName,EParallelForFlags::BackgroundPriority);
}

void AThreadExamples2_hwGameModeBase::CreateAverageScore()
{
	int32 curSeletedNumber = 0;
	const auto FunctionGenerateName = [&](int32 index)
	{
		FPlatformProcess::Sleep(0.1f);

		// for (int i = 0; i < 50; ++i)
		// {			
			curSeletedNumber = 0;
			curSeletedNumber = GetRandomInt(0, 100);
		
			All_AverageScore.Enqueue(curSeletedNumber);
		//}
	};

	ParallelForTemplate(NumberOfAttempts,FunctionGenerateName,EParallelForFlags::BackgroundPriority);
}

void AThreadExamples2_hwGameModeBase::GeneratedData()
{
	CreateNames();
	CreateAges();
	CreateAverageScore();
}

void AThreadExamples2_hwGameModeBase::CreateStudentsInfo()
{
	// //TArray<FString> CurNames;
	// FString OutItem_Name;
	// //TArray<int32> CurAges;
	// int32 OutItem_Age;
	// //TArray<int32> CurAverageScore;
	// int32 OutItem_AverageScore;

	
	// while (All_Name.Dequeue(OutItem_Name))
	// {
	// 	CurNames.Push(*OutItem_Name);		
	// }
	// while (All_Age.Dequeue(OutItem_Age))
	// {
	// 	CurAges.Add(OutItem_Age);		
	// }
	// while (All_AverageScore.Dequeue(OutItem_AverageScore))
	// {
	// 	CurAges.Add(OutItem_AverageScore);		
	// }

	FStudents NewElementStudentsInfo;
	for (auto i = 0; i< NumberOfAttempts; i++)
	{
		//UE_LOG(LogTemp, Warning, TEXT("CurrentNames.Num() - %s"), CurrentNames.Num());
		
		if (CurrentNames.IsValidIndex(i))
		{
			NewElementStudentsInfo.Name = CurrentNames[i];	
		}
		if (CurrentAges.IsValidIndex(i))
		{
			NewElementStudentsInfo.Age = CurrentAges[i];
		}
		if (CurrentAverageScore.IsValidIndex(i))
		{
			NewElementStudentsInfo.AverageScore = CurrentAverageScore[i];
		}
		StudentsInfo.Add(NewElementStudentsInfo);
	}
}

//TArray<FString> AThreadExamples2_hwGameModeBase::GetNames()
void AThreadExamples2_hwGameModeBase::GetNames()
{
	//TArray<FString> ListNames;
	// FString OutName;
	// while (All_Name.Dequeue(OutName))
	// {
	// 	ListNames.Add(OutName);
	// }
	// CurrentNames.Append(ListNames);
	// return CurrentNames;

	CurrentNames.Empty();
	FString OutName;
	while (All_Name.Dequeue(OutName))
	{
		CurrentNames.Add(OutName);
	}
}

TArray<int32> AThreadExamples2_hwGameModeBase::GetAges()
{
	TArray<int32> ListAges;
	int32 OutAge;
	while (All_Age.Dequeue(OutAge))
	{
		ListAges.Add(OutAge);
	}
	CurrentAges.Append(ListAges);
	return CurrentAges;
}

TArray<int32> AThreadExamples2_hwGameModeBase::GetAverageScope()
{
	TArray<int32> ListAverageScore;
	int32 OutAverageScore;
	while (All_AverageScore.Dequeue(OutAverageScore))
	{
		ListAverageScore.Add(OutAverageScore);
	}
	CurrentAverageScore.Append(ListAverageScore);
	return CurrentAverageScore;
}

int32 AThreadExamples2_hwGameModeBase::GetRandomInt(int32 min, int32 max)
{
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<> distrib(min, max);

	return distrib(gen);
}

void AThreadExamples2_hwGameModeBase::InitListNames()
{
	ListNames.Add("Oleg");
	ListNames.Add("Oksana");
	ListNames.Add("Sergey");
	ListNames.Add("Masha");
	ListNames.Add("Misha");
	ListNames.Add("Sveta");
	ListNames.Add("Anatoliy");
	ListNames.Add("Nastaj");
	ListNames.Add("Andrey");
	ListNames.Add("Natasha");
}



