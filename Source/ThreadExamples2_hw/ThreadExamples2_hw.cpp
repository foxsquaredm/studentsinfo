// Copyright Epic Games, Inc. All Rights Reserved.

#include "ThreadExamples2_hw.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, ThreadExamples2_hw, "ThreadExamples2_hw" );
