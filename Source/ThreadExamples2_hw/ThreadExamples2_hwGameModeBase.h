// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "IMessageContext.h"
#include "StudioAnalytics.h"
#include "GameFramework/GameModeBase.h"
#include "ThreadExamples2_hwGameModeBase.generated.h"

class FMessageEndpoint;

USTRUCT(BlueprintType, Atomic)
struct FStudents
{
	GENERATED_BODY();
	
	//возрастом, именем и средним баллом

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString Name = "none";
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 Age = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 AverageScore = 0;
};

// USTRUCT()
// struct FStructMessage_Name
// {
// 	GENERATED_BODY()
//
// 	FString Name = "none";
// 	
// 	FStructMessage_Name(FString InName = "none") : Name(InName) {};
// };
//
// USTRUCT()
// struct FStructMessage_Age
// {
// 	GENERATED_BODY()
//
// 	int32 Age = 0;
//
// 	FStructMessage_Age(int32 InAge = 0) : Age(InAge) {};
// };
//
// USTRUCT()
// struct FStructMessage_AverageScore
// {
// 	GENERATED_BODY()
//
// 	int32 AverageScore = 0;
//
// 	FStructMessage_AverageScore(int32 InScore = 0) : AverageScore(InScore) {};
// };



// DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnUpdate_ByName, FString, curName);
// DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnUpdate_ByAge, int32, curAge);
// DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnUpdate_ByAverageScore, int32, curAverageScore);
// DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnUpdate_ByStudent, FStudents, NewStudent);

UCLASS()
class THREADEXAMPLES2_HW_API AThreadExamples2_hwGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	
public:
	// UPROPERTY(BlueprintAssignable)
	// FOnUpdate_ByName OnUpdate_ByName;
	// UPROPERTY(BlueprintAssignable)
	// FOnUpdate_ByAge OnUpdate_ByAge;
	// UPROPERTY(BlueprintAssignable)
	// FOnUpdate_ByAge OnUpdate_ByAverageScore;
	// UPROPERTY(BlueprintAssignable)
	// FOnUpdate_ByStudent OnUpdate_ByStudent;

	// TArray<FRunnableThread*> curRunningThread;
	// FRunnableThread* curRunningThread_SimpleCollectable = nullptr;
	
	TQueue<FString,EQueueMode::Mpsc> All_Name;
	TQueue<int32,EQueueMode::Mpsc> All_Age;
	TQueue<int32,EQueueMode::Mpsc> All_AverageScore;

	UPROPERTY(BlueprintReadWrite)
	TArray<FStudents> StudentsInfo;

	int32 NumberOfAttempts = 10;
	
	// TSharedPtr<FMessageEndpoint, ESPMode::ThreadSafe> ReciveEndpoint_Name;
	// TSharedPtr<FMessageEndpoint, ESPMode::ThreadSafe> ReciveEndpoint_Age;
	// TSharedPtr<FMessageEndpoint, ESPMode::ThreadSafe> ReciveEndpoint_AverageScore;
	// TSharedPtr<FMessageEndpoint, ESPMode::ThreadSafe> ReciveEndpoint_Students;

	// void BusMessageHandler_Name(const struct FStructMessage_Name& Message,	const TSharedRef<IMessageContext, ESPMode::ThreadSafe>& Context);
	// void BusMessageHandler_Age(const struct FStructMessage_Age& Message,	const TSharedRef<IMessageContext, ESPMode::ThreadSafe>& Context);
	// void BusMessageHandler_AverageScore(const struct FStructMessage_AverageScore& Message, const TSharedRef<IMessageContext, ESPMode::ThreadSafe>& Context);
	// void BusMessageHandler_Students(const struct FStudents& Message,		const TSharedRef<IMessageContext, ESPMode::ThreadSafe>& Context);
	//
	// void EventMessage_Name(FString Name);
	// void EventMessage_Age(int32 Age);
	// void EventMessage_AverageScore(int32 AverageScore);
	// void EventMessage_Students(FStudents Student);

	UFUNCTION(BlueprintCallable, Category = "ThreadTest")
		void CreateNames();
	UFUNCTION(BlueprintCallable, Category = "ThreadTest")
		void CreateAges();
	UFUNCTION(BlueprintCallable, Category = "ThreadTest")
		void CreateAverageScore();
	UFUNCTION(BlueprintCallable, Category = "ThreadTest")
		void GeneratedData();
	
	UFUNCTION(BlueprintCallable, Category = "ThreadTest")
		void CreateStudentsInfo();

	UPROPERTY(BlueprintReadWrite)
	TArray<FString> CurrentNames;
	UPROPERTY(BlueprintReadWrite)
	TArray<int32> CurrentAges;
	UPROPERTY(BlueprintReadWrite)
	TArray<int32> CurrentAverageScore;
	
	UFUNCTION(BlueprintCallable)
		//TArray<FString> GetNames();
		void GetNames();
	UFUNCTION(BlueprintCallable)
		TArray<int32> GetAges();
	UFUNCTION(BlueprintCallable)
		TArray<int32> GetAverageScope();

	
	UFUNCTION(BlueprintCallable, Category = "ThreadTest")
		int32 GetRandomInt(int32 min, int32 max);
	TArray<FString> ListNames;
		void InitListNames();
	
};
